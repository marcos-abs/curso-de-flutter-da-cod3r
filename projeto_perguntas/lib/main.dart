/*
 * *****
 * File: main.dart
 * Project: lib
 * File Created: Tuesday, 15 June 2021 19:22:41
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 29 June 2021 17:44:37
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Primeira aplicação do Dart
 * *****
 */

import 'package:flutter/material.dart';
import './questao.dart';

main() => runApp(PerguntaApp());

class _PerguntaAppState extends State<PerguntaApp> {
  var _perguntaSelecionada = 0;

  void _responder() {
    setState(() {
      _perguntaSelecionada++;
    });
    print(_perguntaSelecionada);
  }

  @override
  Widget build(BuildContext context) {
    // final perguntas = []; // também funciona assim (!!).
    final List<String> perguntas = [
      'Qual é a sua cor favorita?',
      'Qual é o seu animal favorito?'
    ];
    // return new MaterialApp();
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Perguntas'),
          ),
          body: Column(children: <Widget>[
            Questao(perguntas[
                // ignore: todo
                _perguntaSelecionada]), //TODO: Parei aqui em 3m02s em 23. Criando Widget Questão
            ElevatedButton(
              child: Text('Resposta 1'),
              onPressed: _responder,
            ), // RaiseButton deprecated.
            ElevatedButton(
              child: Text('Resposta 2'),
              onPressed: _responder,
            ), // RaiseButton deprecated
            ElevatedButton(
              child: Text('Resposta 3'),
              onPressed: _responder,
            ), // RaiseButton deprecated.
          ])),
    );
  }
}

class PerguntaApp extends StatefulWidget {
  @override
  _PerguntaAppState createState() {
    return _PerguntaAppState();
  }
}
