/*
 * *****
 * File: exercicio.dart
 * Project: exercicios_dart
 * File Created: Tuesday, 15 June 2021 19:58:11
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 15 June 2021 22:29:44
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Primeira aplicação em Dart (ainda sem Flutter, rs)
 * *****
 */

imprimirProduto(int qtde, {required String nome, double? preco}) {
  // require ou ? são formas de resolver o problema - "The parameter 'valor' can't have a value of 'null' ".
  for (var i = 0; i < qtde; i++) {
    print("O produto $nome tem o preço R\$$preco");
  }
}

class Produto {
  late String nome;
  late double preco;

  Produto({required this.nome, this.preco = 9.99});
}

main() {
  var p1 = Produto(nome: 'Lapis');
  var p2 = Produto(preco: 1454.99, nome: 'Geladeira');

  imprimirProduto(2, nome: p1.nome, preco: p1.preco);
  imprimirProduto(1, preco: p2.preco, nome: p2.nome);
}
